﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework
{
    using System.IO;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    public static class DomainContextConfigurator
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services, string connectionString)
        {
            if (!Directory.Exists("Database"))
            {
                Directory.CreateDirectory("Database");
            }

            services.AddDbContext<DomainContext>(ob =>
                ob.UseSqlite(connectionString));

            return services;
        }
    }
}
