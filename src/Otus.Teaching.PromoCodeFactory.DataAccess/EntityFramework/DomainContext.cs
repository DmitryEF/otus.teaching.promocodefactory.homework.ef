﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework
{
    using Microsoft.EntityFrameworkCore;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

    public class DomainContext : DbContext
    {
        public DomainContext(DbContextOptions<DomainContext> options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cs => cs.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(k => k.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(p => p.Preference)
                .WithMany(p => p.CustomerPreferences)
                .HasForeignKey(k => k.PreferenceId);
            
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DomainContext).Assembly);
        }
    }
}
