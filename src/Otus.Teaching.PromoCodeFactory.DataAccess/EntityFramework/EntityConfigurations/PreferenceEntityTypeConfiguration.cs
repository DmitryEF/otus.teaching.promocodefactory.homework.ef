﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework.EntityConfigurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

    public class PreferenceEntityTypeConfiguration: IEntityTypeConfiguration<Preference>
    {
        public void Configure(EntityTypeBuilder<Preference> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(50);
        }
    }
}
