﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework.EntityConfigurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

    public class RoleEntityTypeConfiguration: IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(50);
            builder.Property(p => p.RoleDescription).HasMaxLength(300);
        }
    }
}
