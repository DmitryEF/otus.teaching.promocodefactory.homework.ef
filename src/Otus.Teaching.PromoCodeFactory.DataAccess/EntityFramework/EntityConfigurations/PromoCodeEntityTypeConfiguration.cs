﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework.EntityConfigurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

    public class PromoCodeEntityTypeConfiguration: IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.Property(p => p.Code).HasMaxLength(50);
            builder.Property(p => p.PartnerName).HasMaxLength(50);
        }
    }
}
