﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework.EntityConfigurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

    public class CustomerEntityTypeConfiguration: IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.Property(e => e.Email).HasMaxLength(50);
            builder.Property(e => e.FirstName).HasMaxLength(50);
            builder.Property(e => e.LastName).HasMaxLength(50);
        }
    }
}
