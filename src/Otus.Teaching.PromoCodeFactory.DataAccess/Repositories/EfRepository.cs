﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
    using Otus.Teaching.PromoCodeFactory.Core.Domain;
    using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;

    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DomainContext domainContext;
        protected readonly DbSet<T> dbSet;

        public EfRepository(DomainContext domainContext)
        {
            this.domainContext = domainContext;
            this.dbSet = this.domainContext.Set<T>();
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await this.dbSet.FindAsync(id);
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await this.dbSet.ToListAsync();
        }

        public async Task<T> AddAsync(T entity)
        {
            var result = await this.dbSet.AddAsync(entity);

            await this.SaveChangesAsync();

            return result.Entity;
        }

        public async Task UpdateAsync(T entity)
        {
            this.dbSet.Entry(entity).State = EntityState.Modified;

            await this.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            this.dbSet.Entry(entity).State = EntityState.Deleted;

            await this.SaveChangesAsync();
        }

        public async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await this.domainContext.SaveChangesAsync(cancellationToken);
        }
    }
}
