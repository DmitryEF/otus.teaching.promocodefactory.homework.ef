﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;

    public class CustomerRepository: EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DomainContext domainContext) : base(domainContext)
        {
        }

        public override async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await this.dbSet
                .Include(s => s.PromoCodes)
                .Include(s => s.CustomerPreferences).ThenInclude( r => r.Preference)
                .ToListAsync();
        }
        
        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            return await this.dbSet
                .Include(s => s.CustomerPreferences)
                .ThenInclude(r => r.Preference)
                .Include(s => s.PromoCodes).FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<Guid>> GetIdsByPreferenceName(string preferenceName)
        {
            return await this.dbSet
                    .Include(p => p.CustomerPreferences)
                    .Where(p => p.CustomerPreferences.Any(c => c.Preference.Name == preferenceName))
                    .Select(c => c.Id)
                .ToListAsync();
        }
    }
}
