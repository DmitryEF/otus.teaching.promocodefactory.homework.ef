﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;

    public class PreferenceRepository : EfRepository<Preference>, IPreferenceRepository
    {
        public PreferenceRepository(DomainContext domainContext) : base(domainContext)
        {
        }

        public async Task<Guid?> GetIdByName(string name)
        {
            return (await this.dbSet.FirstOrDefaultAsync(p => p.Name == name))?.Id;
        }

        public async Task<bool> Exists(Guid id)
        {
            return await this.dbSet.AnyAsync(p => p.Id == id);
        }
    }

}