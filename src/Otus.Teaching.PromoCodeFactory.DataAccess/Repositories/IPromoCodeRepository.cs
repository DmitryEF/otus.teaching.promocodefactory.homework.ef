﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

public interface IPromoCodeRepository
{
    Task<IEnumerable<PromoCode>> GetAllAsync();

    Task<PromoCode> AddAsync(PromoCode entity);
}