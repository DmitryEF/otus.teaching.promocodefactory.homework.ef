﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;

    public class CustomerPreferenceRepository : EfRepository<CustomerPreference>, ICustomerPreferenceRepository
    {
        public CustomerPreferenceRepository(DomainContext domainContext) : base(domainContext)
        {
        }
    }
}
