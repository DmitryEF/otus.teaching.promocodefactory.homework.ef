﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

using System;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

public interface ICustomerPreferenceRepository
{
    Task<CustomerPreference> GetByIdAsync(Guid id);

    Task<CustomerPreference> AddAsync(CustomerPreference entity);

    Task DeleteAsync(CustomerPreference entity);
}