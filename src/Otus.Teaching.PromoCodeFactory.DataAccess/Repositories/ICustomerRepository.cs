﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

public interface ICustomerRepository
{
    Task<IEnumerable<Customer>> GetAllAsync();

    Task<Customer> GetByIdAsync(Guid id);

    Task<Customer> AddAsync(Customer entity);

    Task UpdateAsync(Customer entity);

    Task DeleteAsync(Customer entity);

    Task<IEnumerable<Guid>> GetIdsByPreferenceName(string preferenceName);

    Task SaveChangesAsync(CancellationToken cancellationToken = default);
}