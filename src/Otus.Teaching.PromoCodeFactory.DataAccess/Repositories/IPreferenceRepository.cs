﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

public interface IPreferenceRepository
{
    Task<IEnumerable<Preference>> GetAllAsync();

    Task<Guid?> GetIdByName(string name);

    Task<bool> Exists(Guid id);
}