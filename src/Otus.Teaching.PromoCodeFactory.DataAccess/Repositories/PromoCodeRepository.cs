﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;

    public class PromoCodeRepository: EfRepository<PromoCode>, IPromoCodeRepository
    {
        public PromoCodeRepository(DomainContext domainContext) : base(domainContext)
        {
        }
    }
}
