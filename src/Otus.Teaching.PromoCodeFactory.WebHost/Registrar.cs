﻿namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    using AutoMapper;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
    using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
    using Otus.Teaching.PromoCodeFactory.WebHost.Mappings;
    using Otus.Teaching.PromoCodeFactory.WebHost.Settings;
    using IConfigurationProvider = AutoMapper.IConfigurationProvider;

    public static class Registrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var appSettings = configuration.Get<ApplicationSettings>();

            services.AddSingleton(appSettings);

            return services.AddSingleton((IConfigurationRoot) configuration)
                .ConfigureContext(appSettings.ConnectionString)
                .AddMappings()
                .AddRepositories();
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IPreferenceRepository, PreferenceRepository>();
            services.AddScoped<IPromoCodeRepository, PromoCodeRepository>();
            services.AddScoped<ICustomerPreferenceRepository, CustomerPreferenceRepository>();

            return services;
        }

        public static IServiceCollection AddMappings(this IServiceCollection services)
        {
            services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));

            return services;
        }

        private static IConfigurationProvider GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CustomerMappingsProfile>();
                cfg.AddProfile<PreferenceMappingsProfile>();
                cfg.AddProfile<PromoCodeMappingsProfile>();
            });

            configuration.AssertConfigurationIsValid();

            return configuration;
        }
    }
}
