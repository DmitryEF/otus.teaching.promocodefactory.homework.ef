﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;

    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : Controller
    {
        private readonly IPreferenceRepository preferenceRepository;
        private readonly IMapper mapper;

        public PreferencesController(IPreferenceRepository preferenceRepository, IMapper mapper)
        {
            this.preferenceRepository = preferenceRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Получение списка предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            return this.Ok(this.mapper.Map<IEnumerable<PreferenceResponse>>(
                await this.preferenceRepository.GetAllAsync()));
        }
    }
}
