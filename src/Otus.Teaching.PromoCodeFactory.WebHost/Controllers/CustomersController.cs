﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
    using Otus.Teaching.PromoCodeFactory.WebHost.Mappings.Extensions;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;

    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerRepository customerRepository;
        private readonly ICustomerPreferenceRepository customerPreferenceRepository;
        private readonly IPreferenceRepository preferenceRepository;
        private readonly IMapper mapper;

        public CustomersController(
            ICustomerRepository customerRepository,
            ICustomerPreferenceRepository customerPreferenceRepository,
            IMapper mapper, 
            IPreferenceRepository preferenceRepository)
        {
            this.customerRepository = customerRepository;
            this.customerPreferenceRepository = customerPreferenceRepository;
            this.mapper = mapper;
            this.preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получение списка всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetCustomersAsync()
        {
            return this.Ok(this.mapper.Map<IEnumerable<CustomerShortResponse>>(
                await this.customerRepository.GetAllAsync()));
        }
        
        /// <summary>
        /// Получение клиента по id
        /// </summary>
        /// <param name="id">id клиента в виде GUID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomerAsync(Guid id)
        {
            var customer = await this.customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return this.BadRequest("Не существует клиента с таким id!");
            }

            return this.Ok(
                (await this.customerRepository.GetByIdAsync(id))
                    .ToCustomerResponse(this.mapper));
        }
        
        /// <summary>
        /// Добавление нового клиента
        /// </summary>
        /// <param name="request">Запрос на добавление</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            return this.Ok(
                (await this.customerRepository.AddAsync(request.ToCustomer()))
                    .ToCustomerShortResponse());
        }

        /// <summary>
        /// Редактирование данных клиента
        /// </summary>
        /// <param name="id">id клиента в виде GUID</param>
        /// <param name="request">Запрос на редактирование</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await this.customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return this.BadRequest("Не существует клиента с таким id!");
            }

            var needToDeleteRelations = request.PreferenceIds.Any(
                requestPreferenceId => 
                    customer.CustomerPreferences
                        .All(c => c.PreferenceId != requestPreferenceId && 
                                  this.preferenceRepository.Exists(requestPreferenceId).Result));

            if (needToDeleteRelations)
            {
                foreach (var customerCustomerPreference in customer.CustomerPreferences)
                {
                    await this.customerPreferenceRepository.DeleteAsync(customerCustomerPreference);
                }

                foreach (var requestPreferenceId in request.PreferenceIds)
                {
                    await this.customerPreferenceRepository.AddAsync(new CustomerPreference()
                    {
                        CustomerId = customer.Id,
                        PreferenceId = requestPreferenceId
                    });
                }
            }

            await this.customerRepository.UpdateAsync(request.ToCustomer(customer));

            return this.Ok("success!");
        }

        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id">id клиента в виде GUID</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await this.customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return this.BadRequest("Не существует клиента с таким id!");
            }

            await this.customerRepository.DeleteAsync(customer);

            return this.Ok("success!");
        }
    }
}