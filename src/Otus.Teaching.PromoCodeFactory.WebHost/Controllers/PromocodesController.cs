﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    using AutoMapper;
    using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
    using Otus.Teaching.PromoCodeFactory.WebHost.Mappings.Extensions;

    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IPromoCodeRepository promoCodeRepository;
        private readonly ICustomerRepository customerRepository;
        private readonly IPreferenceRepository preferenceRepository;
        private readonly IMapper mapper;

        public PromocodesController(
            IPromoCodeRepository promoCodeRepository, 
            ICustomerRepository customerRepository, 
            IPreferenceRepository preferenceRepository, 
            IMapper mapper)
        {
            this.promoCodeRepository = promoCodeRepository;
            this.customerRepository = customerRepository;
            this.preferenceRepository = preferenceRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            return this.mapper.Map<List<PromoCodeShortResponse>>(
                await this.promoCodeRepository.GetAllAsync());
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferenceId = await this.preferenceRepository.GetIdByName(request.Preference);

            if (preferenceId == null)
            {
                return this.BadRequest($"Предпочтения с названием {request.Preference} не существует!");
            }

            var customerIds = await this.customerRepository.GetIdsByPreferenceName(request.Preference);

            if (customerIds == null) return this.Ok($"Клиенты с предпочтением \"{request.Preference}\" не найдены");

            var beginDate = DateTime.Now.AddDays(-3);
            var endDate = DateTime.Now.AddDays(3);

            foreach (var customerId in customerIds)
            {
                await this.promoCodeRepository.AddAsync(request.ToPromoCode(customerId, (Guid)preferenceId, beginDate, endDate));
            }

            return this.Ok($"Промокод \"{request.PromoCode}\" успешно добавлен клиентам с предпочтением {request.Preference}");
        }
    }
}