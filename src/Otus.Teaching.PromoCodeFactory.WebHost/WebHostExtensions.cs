﻿namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    using System;
    using System.Linq;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;

    public static class WebHostExtensions
    {
        public static IHost InitDatabase(this IHost host)
        {
            using var scope = host.Services.CreateScope();

            var services = scope.ServiceProvider;

            var dbContext = services.GetService<DomainContext>();

            if (!dbContext.Preferences.Any())
                InitDatabaseFromFakeRepository(dbContext, services);

            return host;
        }

        private static void InitDatabaseFromFakeRepository(DomainContext dbContext, IServiceProvider services)
        {
            var roleRepository = services.GetService<IRepository<Role>>();

            var preferenceRepository = services.GetService<IRepository<Preference>>();

            var employeeRepository = services.GetService<IRepository<Employee>>();

            var customerRepository = services.GetService<IRepository<Customer>>();

            var promoCodeRepository = services.GetService<IRepository<PromoCode>>();

            //For delete database if it`s not initialized (commented because task #8)
            //dbContext.Database.EnsureDeleted();

            //dbContext.Database.EnsureCreated();

            dbContext.Roles.AddRange(roleRepository.GetAllAsync().Result);

            dbContext.Preferences.AddRange(preferenceRepository.GetAllAsync().Result);

            foreach (var employee in employeeRepository.GetAllAsync().Result)
            {
                dbContext.Employees.Add(
                    new Employee()
                    {
                        Id = employee.Id,
                        RoleId = employee.Role.Id,
                        FirstName = employee.FirstName,
                        LastName = employee.LastName,
                        Email = employee.Email,
                        AppliedPromocodesCount = employee.AppliedPromocodesCount
                    }
                );
            }

            dbContext.Customers.AddRange(customerRepository.GetAllAsync().Result);

            dbContext.PromoCodes.AddRange(promoCodeRepository.GetAllAsync().Result);

            dbContext.SaveChanges();
        }
    }
}
