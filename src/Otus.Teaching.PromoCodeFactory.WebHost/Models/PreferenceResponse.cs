﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    using System;

    public class PreferenceResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
