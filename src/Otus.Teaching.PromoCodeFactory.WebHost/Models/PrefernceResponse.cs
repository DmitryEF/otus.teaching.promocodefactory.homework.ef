﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    using System;

    public class PrefernceResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
