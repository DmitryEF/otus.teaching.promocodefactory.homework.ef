﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappings
{
    using AutoMapper;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;

    public class CustomerMappingsProfile: Profile
    {
        public CustomerMappingsProfile()
        {
            CreateMap<Customer, CustomerShortResponse>();
        }
    }
}
