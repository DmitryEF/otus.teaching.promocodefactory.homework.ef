﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappings.Extensions
{
    public static class StringExtensions
    {
        public static string SetIfNotNullOrEmpty(this string source, string value)
        {
            return string.IsNullOrEmpty(value) ? source : value;
        }
    }
}
