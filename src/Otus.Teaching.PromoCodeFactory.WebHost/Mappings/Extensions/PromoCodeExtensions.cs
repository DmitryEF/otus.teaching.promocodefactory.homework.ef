﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappings.Extensions
{
    using System;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;

    public static class PromoCodeExtensions
    {
        public static PromoCode ToPromoCode(this GivePromoCodeRequest givePromoCodeRequest, Guid customerId,
            Guid preferenceId, DateTime beginDateTime, DateTime enDateTime)
        {
            return new PromoCode
            {
                ServiceInfo = givePromoCodeRequest.ServiceInfo,
                PartnerName = givePromoCodeRequest.PartnerName,
                Code = givePromoCodeRequest.PromoCode,
                PreferenceId = preferenceId,
                CustomerId = customerId,
                BeginDate = beginDateTime,
                EndDate = enDateTime,
                PartnerManagerId = null
            };
        }
    }
}
