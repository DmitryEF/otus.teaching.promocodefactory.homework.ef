﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappings.Extensions
{
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;

    public static class CustomerExtensions
    {
        public static CustomerResponse ToCustomerResponse(this Customer customer, IMapper mapper)
        {
            var preferencesResponse =
                customer.CustomerPreferences.Select(
                    preference =>
                        new PreferenceResponse
                        {
                            Id = preference.PreferenceId,
                            Name = preference.Preference?.Name
                        }).ToList();

            return new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = preferencesResponse,
                PromoCodes = mapper.Map<ICollection<PromoCodeShortResponse>>(customer.PromoCodes)
            };
        }
        
        public static CustomerShortResponse ToCustomerShortResponse(this Customer customer)
        {
            return new CustomerShortResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
            };
        }

        public static Customer ToCustomer(this CreateOrEditCustomerRequest createOrEditRequest, Customer existingCustomer = null)
        {
            var customer = existingCustomer ?? new Customer();

            customer.FirstName = customer.FirstName.SetIfNotNullOrEmpty(createOrEditRequest.FirstName);
            customer.LastName = customer.LastName.SetIfNotNullOrEmpty(createOrEditRequest.LastName);
            customer.Email = customer.Email.SetIfNotNullOrEmpty(createOrEditRequest.Email);

            if (existingCustomer == null && createOrEditRequest.PreferenceIds != null && createOrEditRequest.PreferenceIds.Any())
            {
                customer.CustomerPreferences = createOrEditRequest.PreferenceIds
                    .Select(guid => 
                        new CustomerPreference { CustomerId = customer.Id, PreferenceId = guid })
                    .ToList();
            }

            return customer;
        }
    }
}
