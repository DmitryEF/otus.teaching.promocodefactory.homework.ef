﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappings
{
    using AutoMapper;
    using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
    using Otus.Teaching.PromoCodeFactory.WebHost.Models;

    public class PreferenceMappingsProfile: Profile
    {
        public PreferenceMappingsProfile()
        {
            CreateMap<Preference, CustomerPreference>()
                .ForMember(c => c.PreferenceId, map => map.MapFrom(m => m.Id))
                .ForAllMembers(m => m.Ignore());

            CreateMap<Preference, PreferenceResponse>();
        }
    }
}
