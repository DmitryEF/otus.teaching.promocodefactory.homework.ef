﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        [ForeignKey("RoleId")]
        public Guid RoleId { get; set; }
        public Role Role { get; set; }

        public int AppliedPromocodesCount { get; set; }

        public ICollection<PromoCode> PromoCodes { get; set; }
    }
}