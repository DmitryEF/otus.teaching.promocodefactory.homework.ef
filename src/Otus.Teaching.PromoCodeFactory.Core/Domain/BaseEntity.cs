﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
    }
}